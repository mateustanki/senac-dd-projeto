/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.dd.senac.dd.projeto.produto;

import java.util.Date;
import java.util.Objects;

/**
 *
 * @author Aluno
 */
public class GrupoProduto {
   private Integer idGrupoProduto;
   private String nomeGrupoProduto;
   private TipoProduto tipoProduto;
   private Float PercDesconto;
   private Date dataExclusao;
   private Date dataInclusao;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.idGrupoProduto);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GrupoProduto other = (GrupoProduto) obj;
        if (!Objects.equals(this.idGrupoProduto, other.idGrupoProduto)) {
            return false;
        }
        return true;
    }

  

    public GrupoProduto() {
    }
    
    
    public GrupoProduto(Integer idGrupoProduto, String nomeGrupoProduto, TipoProduto tipoProduto) {
        this.idGrupoProduto = idGrupoProduto;
        this.nomeGrupoProduto = nomeGrupoProduto;
        this.tipoProduto = tipoProduto;
    }

    /**
     * @return the idGrupoProduto
     */
    public Integer getIdGrupoProduto() {
        return idGrupoProduto;
    }

    /**
     * @param idGrupoProduto the idGrupoProduto to set
     */
    public void setIdGrupoProduto(Integer idGrupoProduto) {
        this.idGrupoProduto = idGrupoProduto;
    }

    /**
     * @return the nomeGrupoProduto
     */
    public String getNomeGrupoProduto() {
        return nomeGrupoProduto;
    }

    /**
     * @param nomeGrupoProduto the nomeGrupoProduto to set
     */
    public void setNomeGrupoProduto(String nomeGrupoProduto) {
        this.nomeGrupoProduto = nomeGrupoProduto;
    }

    /**
     * @return the tipoProduto
     */
    public TipoProduto getTipoProduto() {
        return tipoProduto;
    }

    /**
     * @param tipoProduto the tipoProduto to set
     */
    public void setTipoProduto(TipoProduto tipoProduto) {
        this.tipoProduto = tipoProduto;
    }

    @Override
    public String toString() {
        return nomeGrupoProduto;
    }

    public Float getPercDesconto() {
        return PercDesconto;
    }

    public void setPercDesconto(Float PercDesconto) {
        this.PercDesconto = PercDesconto;
    }

    public Date getDataExclusao() {
        return dataExclusao;
    }

    public void setDataExclusao(Date dataExclusao) {
        this.dataExclusao = dataExclusao;
    }

    public Date getDataInclusao() {
        return dataInclusao;
    }

    public void setDataInclusao(Date dataInclusao) {
        this.dataInclusao = dataInclusao;
    }

   
}
