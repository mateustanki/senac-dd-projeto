/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.dd.senac.dd.projeto.produto;

import br.senac.dd.grupoproduto.model.GrupoProdutoDAO;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Aluno
 */
public class GrupoProdutoDAOTeste {
    public static void main(String args[]) throws SQLException{
        GrupoProdutoDAO dao = new GrupoProdutoDAO();
        dao.inserir(null);
        GrupoProduto gpNovo = new GrupoProduto();
        gpNovo.setNomeGrupoProduto("Roupas");
        gpNovo.setTipoProduto(TipoProduto.MERCADORIA);
        Integer idNovoGP = dao.inserir(gpNovo);
        System.out.println("Novo ID" + idNovoGP);
        List<GrupoProduto> lista = dao.listarPorNome("a");
        for(GrupoProduto gp : lista){
            System.out.println(gp.getNomeGrupoProduto());
        }
    }
}