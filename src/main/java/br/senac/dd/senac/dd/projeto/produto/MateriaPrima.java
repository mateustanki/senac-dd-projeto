/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.dd.senac.dd.projeto.produto;

import br.senac.produto.model.Produto;
import java.util.Date;

/**
 *
 * @author Aluno
 */
public class MateriaPrima extends Produto{
    private Long idMateriaPrima;
    private Float percIPI;

    public MateriaPrima() {
    }

    public MateriaPrima(Long idMateriaPrima, Float percIPI) {
        this.idMateriaPrima = idMateriaPrima;
        this.percIPI = percIPI;
    }

    public MateriaPrima(Long idMateriaPrima, Float percIPI, Long idProduto, String nomeProduto, TipoProduto tipoProduto, String descricao, Date dataCriacao, Date dataAlteracao, Float percICMS, GrupoProduto grupoProduto) {
        this.idMateriaPrima = idMateriaPrima;
        this.percIPI = percIPI;
    }

    
    /**
     * @return the idMateriaPrima
     */
    public Long getIdMateriaPrima() {
        return idMateriaPrima;
    }

    /**
     * @param idMateriaPrima the idMateriaPrima to set
     */
    public void setIdMateriaPrima(Long idMateriaPrima) {
        this.idMateriaPrima = idMateriaPrima;
    }

    /**
     * @return the percIPI
     */
    public Float getPercIPI() {
        return percIPI;
    }

    /**
     * @param percIPI the percIPI to set
     */
    public void setPercIPI(Float percIPI) {
        this.percIPI = percIPI;
    }
    
    public Float getTotalPercImposto(){
        return null;
    }
}
