/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.dd.senac.dd.projeto.produto;

import br.senac.produto.model.Produto;
import java.util.Date;



/**
 *
 * @author Aluno
 */
public class Mercadoria extends Produto{
  private Long idMercadoria;
  private byte[] imagem;

    public Mercadoria() {
    }

    public Mercadoria(Long idMercadoria, byte[] imagem) {
        this.idMercadoria = idMercadoria;
        this.imagem = imagem;
    }

    public Mercadoria(Long idMercadoria, byte[] imagem, Long idProduto, String nomeProduto, TipoProduto tipoProduto, String descricao, Date dataCriacao, Date dataAlteracao, Float percICMS, GrupoProduto grupoProduto) {
       
        this.idMercadoria = idMercadoria;
        this.imagem = imagem;
    }

  
  
    /**
     * @return the idMercadoria
     */
    public Long getIdMercadoria() {
        return idMercadoria;
    }

    /**
     * @param idMercadoria the idMercadoria to set
     */
    public void setIdMercadoria(Long idMercadoria) {
        setIdProduto(idMercadoria);
        this.idMercadoria = idMercadoria;
    }

    /**
     * @return the imagem
     */
    public byte[] getImagem() {
        return imagem;
    }

    /**
     * @param imagem the imagem to set
     */
    public void setImagem(byte[] imagem) {
        this.imagem = imagem;
    }
    
  @Override
    public Float getTotalPercImposto(){
        return null;
    }
}
