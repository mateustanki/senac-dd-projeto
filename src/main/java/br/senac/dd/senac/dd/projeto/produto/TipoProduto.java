/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.dd.senac.dd.projeto.produto;

/**
 *
 * @author Aluno
 */
public enum TipoProduto {
    MERCADORIA(1), MATERIA_PRIMA(2),SERVICO(3);
    
    private Integer id;
    
    private TipoProduto(Integer id) {
        this.id = id;
    }
    
    public Integer getId(){
        return id;
    }
}
