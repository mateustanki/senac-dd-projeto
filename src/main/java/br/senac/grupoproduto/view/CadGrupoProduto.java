
package br.senac.grupoproduto.view;

import br.senac.dd.grupoproduto.model.GrupoProdutoDAO;
import br.senac.dd.senac.dd.projeto.produto.GrupoProduto;
import br.senac.dd.senac.dd.projeto.produto.TipoProduto;
import java.awt.BorderLayout;

import java.awt.ComponentOrientation;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;


public class CadGrupoProduto extends JDialog {
    public static void main(String[] args) {
        CadGrupoProduto cadastro = new CadGrupoProduto();
        cadastro.setVisible(true);
    }
    private JTextField txtNome;
    private JTextField txtCodigo;
    private JRadioButton btnServ;
    private JRadioButton btnMerc;
    private JRadioButton btnMateria;
    
    private JPanel pnBotoes; 
    private JPanel pnDados; 
    private JPanel pnTipoProd;
    private boolean edicao;
    private GrupoProduto grupoProd;
    private GrupoProdutoDAO grupoProdDAO = new GrupoProdutoDAO();
    
   
    
    public CadGrupoProduto(){
        configurarTela();
        configurarPanelBotoes();
        configurarPanelTipoProduto();
        configurarPanelDados();

    }
    
    private void configurarTela() {
        //Configurações gerais da tela: título, tamanho, defaulCloseOperation etc
        //Configurar o Layout da tela principal
        setTitle("Cadastrar Grupo do Produto");
        setSize(700,350);
        setLocationRelativeTo(null);
        setLayout(new BorderLayout());
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
     
    }
     private void configurarPanelTipoProduto() {
        //Criar JPanel e setar o Layout. Criar os Radio Buttons e adicionar no panel. 
        //Criar um objeto do tipo ButtonGroup e adicionar os Radio Buttons nele
        pnTipoProd = new JPanel(new FlowLayout());
        btnServ = new JRadioButton("Serviço");
        btnMerc = new JRadioButton("Mercadoria");
        btnMateria = new JRadioButton("Matéria Prima");
        
        ButtonGroup btGroup = new ButtonGroup();
        btGroup.add(btnServ);
        btGroup.add(btnMerc);
        btGroup.add(btnMateria);
        
       
        pnTipoProd.add(btnServ);
        pnTipoProd.add(btnMerc);
        pnTipoProd.add(btnMateria);
        
     }

    private void configurarPanelBotoes() {
        //Criar o JPanel e setar o Layout. Criar os dois botões e adicionar no panel 
        pnBotoes = new JPanel();
        pnBotoes.setLayout(new BoxLayout(pnBotoes, BoxLayout.LINE_AXIS));
        pnBotoes.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        
        add(pnBotoes, BorderLayout.SOUTH);
        JButton btGravar = new JButton("Gravar");
        JButton btCancelar = new JButton("Cancelar");
        pnBotoes.add(btGravar);
        pnBotoes.add(btCancelar);
        
        btGravar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    gravar();
                } catch (SQLException ex) {
                    Logger.getLogger(CadGrupoProduto.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }
        
    public void gravar() throws SQLException{
        Integer integerCod =  null;
        if(!txtCodigo.getText().equals("")){
         integerCod = new Integer(txtCodigo.getText());
        //pegar id que está em String e transfireri para Integer
        }
        if(grupoProd == null){
            grupoProd = new GrupoProduto();
        }
        
        if(txtNome.getText().trim().equals("")){
            JOptionPane.showMessageDialog(this,"Digite nome do produto","Atenção",JOptionPane.WARNING_MESSAGE);
            txtNome.requestFocus();
            return;
        }
        

        if(btnMateria.isSelected()== false && btnMerc.isSelected() == false 
        && btnServ.isSelected() == false  ){
            //cirar JOPTION ERROR
            JOptionPane.showMessageDialog(this,"Selecione um Tipo de produto","Atenção",JOptionPane.WARNING_MESSAGE);
        }
        
        //data-biding
         if (btnServ.isSelected() == true) {
            grupoProd.setTipoProduto(TipoProduto.SERVICO);
        }else if(btnMerc.isSelected() == true) {
            grupoProd.setTipoProduto(TipoProduto.MERCADORIA);
        }else if(btnMateria.isSelected() == true){
            grupoProd.setTipoProduto(TipoProduto.MATERIA_PRIMA);
        }    
            grupoProd.setNomeGrupoProduto(txtNome.getText());
            grupoProd.setIdGrupoProduto(integerCod);
            
        if(txtCodigo.getText().equals("")){ //inclusão
            Integer novoId = grupoProdDAO.inserir(grupoProd);
            
            JOptionPane.showMessageDialog(this, "O produto "+grupoProd.getNomeGrupoProduto()+"com id "
                    +novoId);
                
        }else { //alteração 
                grupoProdDAO.alterar(grupoProd);
                JOptionPane.showMessageDialog(this, "Alterado com sucesso");        
     }
    }
    

    private void configurarPanelDados() {
        //Criar o JPanel e setar o Layout. Criar os três objetos de JLabel e adicionar no panel.
        //Criar os dois objetos de JTextField e adicionar no panel. Adicionar também o Panel de Botões.
        pnDados = new JPanel(new GridLayout(3, 2));
        add(pnDados, BorderLayout.CENTER);
        
        JLabel lblCodigo = new JLabel(" Nome :");
        txtNome = new JTextField();
        JLabel lblProduto = new JLabel(" Código :");
        txtCodigo = new JTextField();
        JLabel lblTipoProd = new JLabel(" Tipo :");
        
        pnDados.add(lblCodigo);
        pnDados.add(txtNome);
        pnDados.add(lblProduto);
        pnDados.add(txtCodigo);
        pnDados.add(lblTipoProd);
        pnDados.add(pnTipoProd);
        
    }
        public void editar(Integer idGrupoProd){
       
        grupoProd = grupoProdDAO.getPorId(idGrupoProd);

        txtNome.setText(grupoProd.getNomeGrupoProduto());
        txtCodigo.setText(idGrupoProd.toString());

        if (null != grupoProd.getTipoProduto()) switch (grupoProd.getTipoProduto()) {
            case SERVICO:
                btnServ.setSelected(true);
                break;
            case MERCADORIA:
                btnMerc.setSelected(true);
                break;
            case MATERIA_PRIMA:
                btnMateria.setSelected(true);
                break;
            default:
                break;
        }
    }
  }
    
   
