package provaJDBC;

import br.senac.dd.componente.model.BaseDAO;
import br.senac.dd.componentes.db.ConexaoDB;
import br.senac.dd.senac.dd.projeto.produto.Mercadoria;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EstoqueMovimentoDAO implements BaseDAO<EstoqueMovimento, Long>{
    
    
    public static void main(String[] args) throws SQLException {
        EstoqueMovimentoDAO dao = new EstoqueMovimentoDAO();
        EstoqueMovimento estoque = new EstoqueMovimento();
        estoque.setDataMovto(new Date());
        estoque.setTipoMovto(TipoMovimentoEstoque.ENTRADA);
        estoque.setQuantidade(638.2);
        Mercadoria mercadoria = new Mercadoria();
        mercadoria.setIdMercadoria(4L);
        estoque.setProduto(mercadoria);
        estoque.setIdUsuario(6);
        estoque.setObservacoes("fdssfrsd");

    
        Long id = dao.inserir(estoque);
        estoque.setIdMovtoEstoque(id);
        System.out.println(dao.getPorId(id));
        estoque.setTipoMovto(TipoMovimentoEstoque.SAIDA);
        dao.alterar(estoque);
        System.out.println(dao.getPorId(id));
        
        System.out.println(dao.excluir(id));
    }
    
    /**
     * A partir de um ResultSet passado como parâmetro, cujo cursor está posicionado
     * em alguma linha da tabela estoquemovto, faça o DataBinding dos dados do ResultSet 
     * para um objeto do tipo EstoqueMovimento.
     * @param rs
     */
    private EstoqueMovimento getEstoqueMovimento(ResultSet rs) throws SQLException {
        EstoqueMovimento estoqueMovimento = new EstoqueMovimento();
        long id = rs.getLong("idMovtoEstoque");

        estoqueMovimento.setIdMovtoEstoque(id);
        estoqueMovimento.setDataMovto(rs.getDate("dataMovto"));
        estoqueMovimento.setQuantidade(rs.getDouble("quantidade"));
 
        estoqueMovimento.setTipoMovto(TipoMovimentoEstoque.getTipoPorCodigo(rs.getString("tipoMovto")));
        Mercadoria mercadoria = new Mercadoria();
        mercadoria.setIdMercadoria(rs.getLong("idProduto"));
        estoqueMovimento.setProduto(mercadoria);
        estoqueMovimento.setIdUsuario(rs.getInt("idUsuario"));
        estoqueMovimento.setObservacoes(rs.getString("observacoes"));


        return estoqueMovimento;
    }
    

    /**
     * A partir do idMovtoEstoque, utilizando Statement ou PreparedStatement, retorne
     * o objeto do tipo EstoqueMovimento. Utilize o método getEstoqueMovimento para
     * fazer o DataBinding.
     * @param idMovtoEstoque
     * @return 
     * @throws java.sql.SQLException
     */
    @Override
    public EstoqueMovimento getPorId(Long idMovtoEstoque) throws SQLException {
        String sql = "Select * from estoquemovto where idMovtoEstoque = " + idMovtoEstoque;

        Connection conn = ConexaoDB.getInstance().getConnection();
        Statement stm = conn.createStatement();
        ResultSet rs = stm.executeQuery(sql);
        if (rs.next() == false) {
            throw new RuntimeException("Movimento de estoque não encontrado: " + idMovtoEstoque + ".");
        }
        EstoqueMovimento estoqueMovimento = getEstoqueMovimento(rs);
        return estoqueMovimento;
    }


    /**
     * Utilizando Statement ou PreparedStatement atualize o Saldo de Estoque.
     * Se o produto não existir na tabela estoquesaldo, é necessário incluir um registro na
     * tabela estoquesaldo desse mesmo produto. Se o produto existir na tabela estoquesaldo, é necessário 
     * apenas somar a quantidade (se o parâmetro "quantidade" tiver valor positivo, vai aumentar a quantidade no estoque,
     * se for negativo, vai diminuir a quantidade no estoque).
     * @param idMovtoEstoque 
     */
    private void atualizarSaldoEstoque(Long idProduto, Double quantidade) throws SQLException{
        String sql = "select * from estoquesaldo where idProduto = " + idProduto;
        
        Connection conn = ConexaoDB.getInstance().getConnection();
        Statement stm = conn.createStatement();
        ResultSet rs = stm.executeQuery(sql);
        
        if (rs.next() == false) {
            String insert = "INSERT INTO `projeto`.`estoquesaldo`\n" +
                    "(`idProduto`,\n" +
                    "`saldo`)\n" +
                    "VALUES\n" +
                    "("+idProduto+",\n" +
                    ""+quantidade+");";
            int regInseridos = stm.executeUpdate(insert, Statement.RETURN_GENERATED_KEYS); 
        } else {
            double saldo = rs.getDouble("saldo");
            if(saldo >= 0){
                String updatePos = "update estoquesaldo set saldo =" + (saldo + quantidade);
                stm.executeUpdate(updatePos);
            } else {
                String updateNeg = "update estoquesaldo set saldo =" + (saldo - quantidade);
                stm.executeUpdate(updateNeg);
            }
        }
        
    }


    /**
     * Utilize PreparedStatement. 
     * Após inserir (INSERT) um registro na tabela estoquemovto, atualize também a tabela
     * estoquesaldo através do método atualizarSaldoEstoque.    
     * Utilize transação, pois iremos atualizar dois registros de tabelas diferentes, 
     * caso ocorra qualquer exceção, faça o rollback.
     * Todos os campos são obrigatórios, com exceção dos campos idUsuario e observacoes que podem ou não ser nulos (null).
     * @param movtoEstoque
     */
    @Override
    public Long inserir(EstoqueMovimento movtoEstoque) throws SQLException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sql = "INSERT INTO `projeto`.`estoquemovto`\n" +
                "(`quantidade`,\n" +
                "`tipoMovto`,\n" +
                "`dataMovto`,\n" +
                "`idProduto`,\n" +
                "`idUsuario`,\n" +
                "`observacoes`)\n" +
                "VALUES\n" +
                "("+movtoEstoque.getQuantidade()+",\n" +
                "'"+movtoEstoque.getTipoMovto().getCodigo()+"',\n" +
                "'"+sdf.format(movtoEstoque.getDataMovto())+"',\n" +
                ""+movtoEstoque.getProduto().getIdProduto()+",\n" +
                ""+movtoEstoque.getIdUsuario()+",\n" +
                "'"+movtoEstoque.getObservacoes()+"')";
        
        Connection conn = ConexaoDB.getInstance().getConnection();
        Statement stm = conn.createStatement();
        int regInseridos = stm.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
        
        ResultSet rsChaveGerada = stm.getGeneratedKeys();
        rsChaveGerada.next();
        return rsChaveGerada.getLong(1);
    }



    /**
     * Utilize PreparedStatement. 
     * Será necessário buscar a quantidade do movimento que está sendo excluído,
     * depois atualizar o estoquesaldo através do método atualizarSaldoEstoque, por último, 
     * faça a exclusão (DELETE) do registro em estoquemovto conforme idMovtoEstoque passado.
     * Transacione essa operação, caso ocorra alguma exceção, faça rollback.
     * @param idMovtoEstoque
     * @return
     */
    @Override
    public boolean excluir(Long idMovtoEstoque) throws SQLException {
        Connection conn = ConexaoDB.getInstance().getConnection();
        try {
            conn.setAutoCommit(false); //Inicia transação
            String sql = "delete from estoquemovto where idMovtoEstoque = ? ";

    
            PreparedStatement pstm = conn.prepareStatement(sql);
            pstm.setLong(1, idMovtoEstoque);

            int nuMovEsExcluidos = pstm.executeUpdate();
            if (nuMovEsExcluidos == 0) {
                return false;
            }

            conn.commit();
        } catch (Exception e) {
            conn.rollback();
            conn.setAutoCommit(true);

            throw new RuntimeException("Erro ao excluir movimento de estoque: " + e.getMessage() + ".", e);
        }
        return true;
    }



    /**
     * Utilize Statement. 
     * Será necessário buscar a quantidade do movimento que está sendo alterado,
     * caso a quantidade que está no banco de dados for diferente da quantidade
     * passada no atributo movtoEstoque.getQuantidade(), é necessário atualizar a tabela
     * estoquesaldo através do método atualizarSaldoEstoque.
     * Após atualização do saldo, alterar (UPDATE) o registro na tabela estoquemovto. 
     * Utilize transação, se qualquer exceção ocorrer, faça o rollback.
	 * Todos os campos são obrigatórios, com exceção dos campos idUsuario e observacoes que podem ou não ser nulos (null).	
     * @param movtoEstoque
     */
    @Override
    public boolean alterar(EstoqueMovimento movtoEstoque) throws SQLException {
        String sql = "UPDATE `projeto`.`estoquemovto`\n" +
                    "SET\n" +
                    "`quantidade` = ?,\n" +
                    "`tipoMovto` = ?,\n" +
                    "`dataMovto` = ?,\n" +
                    "`idProduto` = ?,\n" +
                    "`idUsuario` = ?,\n" +
                    "`observacoes` = ?\n" +
                    "WHERE `idMovtoEstoque` = ?";
        
        Connection conn = ConexaoDB.getInstance().getConnection();
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setDouble(1, movtoEstoque.getQuantidade());
        ps.setObject(2, movtoEstoque.getTipoMovto().getCodigo().toString());
        ps.setTimestamp(3, new java.sql.Timestamp(movtoEstoque.getDataMovto().getTime()));
        ps.setLong(4, movtoEstoque.getProduto().getIdProduto());
        ps.setLong(5, movtoEstoque.getIdUsuario());
        ps.setString(6, movtoEstoque.getObservacoes());
        ps.setLong(7, movtoEstoque.getIdMovtoEstoque());
        
        int regAlterados = ps.executeUpdate();
        return (regAlterados == 1);
    }
    

    /**
     * Utilizando Statement ou PreparedStatement, a partir do idProduto informado 
     * e dataInicioMovto (considere como DATE apenas), retorne a lista de objetos do tipo EstoqueMovimento.
     * Utilize a função getEstoqueMovimento para fazer o DataBinding dos dados
     * de cada uma das linhas do ResultSet para o objeto EstoqueMovimento.
     * Retorne apenas os registro cujo dataMovto seja maior ou igual a dataInicioMovto.
     * @param idProduto
     */
    public List<EstoqueMovimento> listarPorProduto(Long idProduto, Date dataInicioMovto) throws SQLException{
        List<EstoqueMovimento> listaEstoque = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        String sql = "SELECT * FROM estoquemovto\n"
                + "WHERE idProduto = " + idProduto + " and "
                + "dataMovto >= {ts ´" + sdf.format(dataInicioMovto)+"´}";
         Connection conn = br.senac.dd.componente.model.ConexaoDB.getInstance().getConnection();
        Statement stm = conn.createStatement();
        ResultSet rs = stm.executeQuery(sql);
        EstoqueMovimento estoqueMov;
        while(rs.next()){
            estoqueMov = getEstoqueMovimento(rs);
            listaEstoque.add(estoqueMov);
        }
        return listaEstoque;
    }
    
}
