/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recuperacao;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author mrsta
 */
public class TelaControleDeVendas extends javax.swing.JFrame {

    private PedidoVenda pVenda = new PedidoVenda();
    private PedidoVendaDAO dao = new PedidoVendaDAO();
    private Boolean buscar;
    private Boolean inserir;
    private Long id;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy");
    
    public TelaControleDeVendas() {
        initComponents();
        reset();
    }
    
     private void reset(){
        buscar = true;
        inserir = true;
        inserirEstado(inserir);
        buscarEstado(buscar);
        limparCampos();
    }

    private void buscarEstado(boolean buscar){
        txtPedido.setEnabled(buscar);
        btnSalvar.setEnabled(!buscar);
        btnExcluir.setEnabled(!buscar);
        if(buscar){
            btnBuscar.setText("Buscar");
        } else {
            btnBuscar.setText("Limpar");
        }   
    }
   
    
    private void inserirEstado(boolean inserir){
        txtPedido.setEnabled(inserir);
        btnSalvar.setEnabled(!inserir);
        if(inserir){
            btnInserir.setText("Inserir");
        } else {
            btnInserir.setText("Limpar");
        }   
    }
    
    private void limparCampos(){
        txtPedido.setText("");
        txtCliente.setText("");
        txtNome.setText("");
        txtData.setValue(null);
        cbFormaPagamento.setSelectedItem(null);
        txtValor.setText("");
        buttonGroup1.clearSelection();
        chbFrete.setSelected(false);
        chbTelemarketing.setSelected(false);
        txtObservacao.setText("");
    }
    
    private void buscar() throws SQLException {
        
        pVenda = dao.getPorId(Long.parseLong(txtPedido.getText()));
        
        txtPedido.setText(pVenda.getIdPedido().toString());
        txtCliente.setText(pVenda.getIdPessoa().toString());
        txtNome.setText(dao.getNomeCliente(Long.parseLong(txtPedido.getText())));
        txtData.setValue(pVenda.getDtPedido());
        cbFormaPagamento.setSelectedIndex(pVenda.getFormaPagamento().getId()-1);
        txtValor.setText(pVenda.getVlFrete().toString());
        if(pVenda.getTipoPedidoVenda().getId().equals(2)){
                radOrcamento.setSelected(true);
    
            }else{
                radPedido.setSelected(true);
            }
        chbFrete.setSelected(pVenda.getFreteGratis());
        chbTelemarketing.setSelected(pVenda.getTelemarketing());
        txtObservacao.setText(pVenda.getObservacoes());
    }
    
    private boolean validar() {
        if (txtCliente.getText().trim().equals("")) {
            JOptionPane.showMessageDialog(this,
                    "Digite um id de cliente", "Atenção", JOptionPane.WARNING_MESSAGE);
            txtCliente.requestFocus();
            return false;
        }
        if (txtData.getText().trim().equals("")) {
            JOptionPane.showMessageDialog(this,
                    "Campo de data vazio", "Atenção", JOptionPane.WARNING_MESSAGE);
            txtData.requestFocus();
            return false;
        }
        if (cbFormaPagamento.getSelectedIndex() == -1) {
            JOptionPane.showMessageDialog(this,
                    "Selecione uma forma de pagamento", "Atenção", JOptionPane.WARNING_MESSAGE);
            cbFormaPagamento.requestFocus();
            return false;
        }
        if (!radOrcamento.isSelected() && !radPedido.isSelected()) {
            JOptionPane.showMessageDialog(this,
                    "Selecione uma opção de vendas", "Atenção", JOptionPane.WARNING_MESSAGE);
            return false;
            
        }
        if (txtValor.getText().trim().equals("") && !chbFrete.isSelected()) {
            JOptionPane.showMessageDialog(this,
                    "Escolha um valor de frete", "Atenção", JOptionPane.WARNING_MESSAGE);
            return false;
        }
        return true;
    }
    
    private void salvar(boolean novo) throws SQLException {
        pVenda.setDtPedido((Date) txtData.getValue());
        FormaPagamento fPag = FormaPagamento.getPorId(cbFormaPagamento.getSelectedIndex()+1);
        
        pVenda.setFormaPagamento(fPag);
        pVenda.setIdPessoa(Long.parseLong(txtCliente.getText()));
        pVenda.setFreteGratis(chbFrete.isSelected());
        pVenda.setTelemarketing(chbTelemarketing.isSelected());
        pVenda.setObservacoes(txtObservacao.getText());
        if(radOrcamento.isSelected()){
           pVenda.setTipoPedidoVenda(TipoPedidoVenda.ORCAMENTO);
         }else if(radPedido.isSelected()){
            pVenda.setTipoPedidoVenda(TipoPedidoVenda.PEDIDO);
        }
        pVenda.setVlFrete(Double.parseDouble(txtValor.getText()));

        if(novo){
            id = dao.inserir(pVenda);
            txtNome.setText(dao.getNomeCliente(Long.parseLong(txtPedido.getText())));
            txtPedido.setText(Long.toString(id));
        } else {
            dao.alterar(pVenda);
        }
        
        
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        buttonGroup1 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        txtPedido = new javax.swing.JTextField();
        radOrcamento = new javax.swing.JRadioButton();
        btnBuscar = new javax.swing.JButton();
        btnInserir = new javax.swing.JButton();
        btnExcluir = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        txtCliente = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtData = new javax.swing.JFormattedTextField();
        jLabel4 = new javax.swing.JLabel();
        cbFormaPagamento = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        radPedido = new javax.swing.JRadioButton();
        jLabel6 = new javax.swing.JLabel();
        txtValor = new javax.swing.JFormattedTextField();
        chbTelemarketing = new javax.swing.JCheckBox();
        chbFrete = new javax.swing.JCheckBox();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtObservacao = new javax.swing.JTextArea();
        btnSalvar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        txtNome = new javax.swing.JLabel();

        org.jdesktop.beansbinding.Binding binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, btnCancelar, org.jdesktop.beansbinding.ELProperty.create("${action.enabled}"), this, org.jdesktop.beansbinding.BeanProperty.create("defaultCloseOperation"));
        bindingGroup.addBinding(binding);

        jLabel1.setText("id Pedido:");

        txtPedido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPedidoActionPerformed(evt);
            }
        });

        buttonGroup1.add(radOrcamento);
        radOrcamento.setText("Orçamento");

        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        btnInserir.setText("Inserir");
        btnInserir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInserirActionPerformed(evt);
            }
        });

        btnExcluir.setText("Excluir");
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });

        jLabel2.setText("id Cliente:");

        txtCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtClienteActionPerformed(evt);
            }
        });

        jLabel3.setText("Data Pedido:");

        txtData.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter()));

        jLabel4.setText("Forma Pagamento");

        cbFormaPagamento.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "DINHEIRO", "CARTAO_CREDITO", "CARTAO_DEBITO", "CHEQUE" }));

        jLabel5.setText("Operação:");

        buttonGroup1.add(radPedido);
        radPedido.setText("Pedido");

        jLabel6.setText("Valor Frete:");

        txtValor.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter()));

        chbTelemarketing.setText("Telemarketing");

        chbFrete.setText("Frete Grátis");
        chbFrete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chbFreteActionPerformed(evt);
            }
        });

        jLabel7.setText("Observações:");

        txtObservacao.setColumns(20);
        txtObservacao.setRows(5);
        jScrollPane1.setViewportView(txtObservacao);

        btnSalvar.setText("Salvar");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel3)
                    .addComponent(jLabel2)
                    .addComponent(jLabel5)
                    .addComponent(jLabel4)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(chbTelemarketing)
                        .addGap(32, 32, 32)
                        .addComponent(chbFrete))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(radOrcamento)
                        .addGap(18, 18, 18)
                        .addComponent(radPedido, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 294, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(cbFormaPagamento, javax.swing.GroupLayout.Alignment.LEADING, 0, 150, Short.MAX_VALUE)
                            .addComponent(txtData, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCliente, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtPedido, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGap(39, 39, 39)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnBuscar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnInserir)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnExcluir))
                            .addComponent(txtNome, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(txtValor, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(72, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSalvar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnCancelar)
                .addGap(8, 8, 8))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPedido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(btnBuscar)
                    .addComponent(btnInserir)
                    .addComponent(btnExcluir))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cbFormaPagamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(radOrcamento)
                    .addComponent(radPedido))
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtValor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(chbTelemarketing)
                    .addComponent(chbFrete))
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSalvar)
                    .addComponent(btnCancelar))
                .addContainerGap())
        );

        bindingGroup.bind();

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtPedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPedidoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPedidoActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed

        if(buscar){
            try {
                buscar();
            } catch (SQLException ex) {
                Logger.getLogger(TelaControleDeVendas.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else{
            limparCampos();
        }
        buscar = !buscar;
        if(buscar){
            reset();
        }
        buscarEstado(buscar);
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void txtClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtClienteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtClienteActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        reset();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnInserirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnInserirActionPerformed
        limparCampos();
        btnExcluir.setEnabled(false);
        inserir = !inserir;
        if(inserir){
            reset();
        }
        inserirEstado(inserir);
    }//GEN-LAST:event_btnInserirActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        boolean ok = validar();
        if(ok){
        
        boolean novo;
        novo = txtPedido.getText().length() < 1;
        try {
            salvar(novo);
        } catch (SQLException ex) {
            Logger.getLogger(TelaControleDeVendas.class.getName()).log(Level.SEVERE, null, ex);
        }
        btnExcluir.setEnabled(true);}
        
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        int retorno = JOptionPane.showConfirmDialog(this,"Confirma exclusao?","Exclusao",JOptionPane.YES_NO_OPTION);
        if(retorno == JOptionPane.YES_OPTION){
            id = Long.parseLong(txtPedido.getText());
            try {
                dao.excluir(id);
            } catch (SQLException ex) {
                Logger.getLogger(TelaControleDeVendas.class.getName()).log(Level.SEVERE, null, ex);
            }
        reset();
        btnExcluir.setEnabled(false);}
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void chbFreteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chbFreteActionPerformed
        if(chbFrete.isSelected()){
            txtValor.setEnabled(false);
            txtValor.setText("");
        } else{
            txtValor.setEnabled(true);
            
        }
    }//GEN-LAST:event_chbFreteActionPerformed

    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaControleDeVendas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaControleDeVendas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaControleDeVendas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaControleDeVendas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaControleDeVendas().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnInserir;
    private javax.swing.JButton btnSalvar;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> cbFormaPagamento;
    private javax.swing.JCheckBox chbFrete;
    private javax.swing.JCheckBox chbTelemarketing;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JRadioButton radOrcamento;
    private javax.swing.JRadioButton radPedido;
    private javax.swing.JTextField txtCliente;
    private javax.swing.JFormattedTextField txtData;
    private javax.swing.JLabel txtNome;
    private javax.swing.JTextArea txtObservacao;
    private javax.swing.JTextField txtPedido;
    private javax.swing.JFormattedTextField txtValor;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables
}
